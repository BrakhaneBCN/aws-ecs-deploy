import os

from bitbucket_pipes_toolkit.test import PipeTestCase

task_definition = """
{  
   "containerDefinitions":[  
      {  
         "dnsSearchDomains":[  

         ],
         "logConfiguration":{  
            "logDriver":"awslogs",
            "options":{  
               "awslogs-group":"/ecs/first-run-task-definition",
               "awslogs-region":"us-east-1",
               "awslogs-stream-prefix":"ecs"
            }
         },
         "entryPoint":[  

         ],
         "portMappings":[  
            {  
               "hostPort":80,
               "protocol":"tcp",
               "containerPort":80
            }
         ],
         "command":[  

         ],
         "cpu":256,
         "environment":[  

         ],
         "mountPoints":[  

         ],

         "memoryReservation":512,
         "volumesFrom":[  

         ],
         "essential":true,
         "links":[  

         ],
         "name":"nginx",
         "image":"nginx"
      }
   ],
   "placementConstraints":[  

   ],

   "family":"first-run-task-definition",
   "requiresCompatibilities":[  

   ],
   "volumes":[  

   ]
}
"""


class ECSDeployTestCase(PipeTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        with open('taskDefinition.json', 'w') as td:
            td.write(task_definition)

    def test_update_successful(self):
        service_name = os.getenv('ECS_SERVICE_NAME')
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', 'us-east-1'),
            'ECS_CLUSTER_NAME': os.getenv('ECS_CLUSTER_NAME'),
            'ECS_SERVICE_NAME': service_name,
            'ECS_TASK_FAMILY_NAME': os.getenv('ECS_TASK_FAMILY_NAME'),
            'ECS_TASK_DEFINITION': os.path.join(os.getcwd(), 'taskDefinition.json'),
            'IMAGE_NAME': 'nginx'
        })

        self.assertRegex(
            result, rf'✔ Successfully updated the {service_name} service')

    def test_update_failes_if_cluster_doesnt_exist(self):
        service_name = os.getenv('ECS_SERVICE_NAME')
        no_such_cluster = 'no-such-cluster'
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', 'us-east-1'),
            'ECS_CLUSTER_NAME': no_such_cluster,
            'ECS_SERVICE_NAME': service_name,
            'ECS_TASK_FAMILY_NAME': os.getenv('ECS_TASK_FAMILY_NAME'),
            'ECS_TASK_DEFINITION': os.path.join(os.getcwd(), 'taskDefinition.json'),
            'IMAGE_NAME': 'nginx'
        })

        self.assertRegex(
            result, rf'ECS cluster not found')

    def test_update_failes_if_service_doesnt_exist(self):
        service_name = 'no-such-service'
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', 'us-east-1'),
            'ECS_CLUSTER_NAME': os.getenv('ECS_CLUSTER_NAME'),
            'ECS_SERVICE_NAME': service_name,
            'ECS_TASK_FAMILY_NAME': os.getenv('ECS_TASK_FAMILY_NAME'),
            'ECS_TASK_DEFINITION': os.path.join(os.getcwd(), 'taskDefinition.json'),
            'IMAGE_NAME': 'nginx'
        })

        self.assertRegex(
            result, rf'ECS service not found')

    def test_update_failes_if_service_doesnt_exist(self):
        service_name = 'no-such-service'
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', 'us-east-1'),
            'ECS_CLUSTER_NAME': os.getenv('ECS_CLUSTER_NAME'),
            'ECS_SERVICE_NAME': service_name,
            'ECS_TASK_FAMILY_NAME': os.getenv('ECS_TASK_FAMILY_NAME'),
            'ECS_TASK_DEFINITION': os.path.join(os.getcwd(), 'taskDefinition.json'),
            'IMAGE_NAME': 'nginx'
        })

        self.assertRegex(
            result, rf'ECS service not found')

    def test_pipe_produces_extra_debug_info(self):
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', 'us-east-1'),
            'ECS_CLUSTER_NAME': os.getenv('ECS_CLUSTER_NAME'),
            'ECS_SERVICE_NAME': os.getenv('ECS_SERVICE_NAME'),
            'ECS_TASK_FAMILY_NAME': os.getenv('ECS_TASK_FAMILY_NAME'),
            'ECS_TASK_DEFINITION': os.path.join(os.getcwd(), 'taskDefinition.json'),
            'DEBUG': 'true'
        }, stderr=True)

        self.assertRegex(
            result, rf'DEBUG: Response body:')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        os.remove('taskDefinition.json')


class InvalidTaskDefinition(PipeTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        with open('invalidTaskDefinition.json', 'w') as td:
            td.write('{')


    def test_update_fails_if_invalid_task_definition(self):
        service_name = 'no-such-service'
        no_such_cluster = 'no-such-cluster'
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', 'us-east-1'),
            'ECS_CLUSTER_NAME': os.getenv('ECS_CLUSTER_NAME'),
            'ECS_SERVICE_NAME': service_name,
            'ECS_TASK_FAMILY_NAME': os.getenv('ECS_TASK_FAMILY_NAME'),
            'ECS_TASK_DEFINITION': os.path.join(os.getcwd(), 'invalidTaskDefinition.json'),
            'IMAGE_NAME': 'nginx'
        })

        self.assertRegex(
            result, rf'Failed to parse the task definition file')

    def test_update_fails_if_task_definition_file_doenst_exist(self):
        service_name = 'no-such-service'
        no_such_cluster = 'no-such-cluster'
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', 'us-east-1'),
            'ECS_CLUSTER_NAME': os.getenv('ECS_CLUSTER_NAME'),
            'ECS_SERVICE_NAME': service_name,
            'ECS_TASK_FAMILY_NAME': os.getenv('ECS_TASK_FAMILY_NAME'),
            'ECS_TASK_DEFINITION': os.path.join(os.getcwd(), 'nonExistantTaskDefinition.json'),
            'IMAGE_NAME': 'nginx'
        })

        self.assertRegex(
            result, rf'Not able to find')

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        os.remove('invalidTaskDefinition.json')


class InvalidParametersTaskDefinition(PipeTestCase):
    def setUp(self):
        super().setUpClass()
        with open('invalidTaskDefinitionParams.json', 'w') as td:
            td.write('{"wrong-param-name": "nginx:latest"}')


    def test_update_fails_if_invalid_task_definition(self):
        service_name = 'no-such-service'
        no_such_cluster = 'no-such-cluster'
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', 'us-east-1'),
            'ECS_CLUSTER_NAME': os.getenv('ECS_CLUSTER_NAME'),
            'ECS_SERVICE_NAME': service_name,
            'ECS_TASK_FAMILY_NAME': os.getenv('ECS_TASK_FAMILY_NAME'),
            'ECS_TASK_DEFINITION': os.path.join(os.getcwd(), 'invalidTaskDefinitionParams.json'),
            'IMAGE_NAME': 'nginx'
        })

        self.assertRegex(
            result, rf'ECS task definition parameter validation error')

    def tearDown(self):
        super().tearDownClass()
        os.remove('invalidTaskDefinitionParams.json')
