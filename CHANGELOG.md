# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.0

- minor: Changed pipe to use the full task definition json instead of only using the container definitions

## 0.1.0

- minor: Initial release

