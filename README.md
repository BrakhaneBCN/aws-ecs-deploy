# Bitbucket Pipelines Pipe: AWS ECS deploy

Deploy a containerized application to AWS ECS

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/aws-ecs-deploy:0.2.0
    variables:
      AWS_ACCESS_KEY_ID: '<string>'
      AWS_SECRET_ACCESS_KEY: '<string>'
      AWS_DEFAULT_REGION: '<string>'
      ECS_CLUSTER_NAME: '<string>'
      ECS_SERVICE_NAME: '<string>'
      ECS_TASK_DEFINITION: '<string>'
      ECS_TASK_FAMILY_NAME: '<string>' # Optional
      DEBUG: '<string>' # Optional
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| AWS_ACCESS_KEY_ID (*)              | AWS key id. |
| AWS_SECRET_ACCESS_KEY (*) | AWS secret key. |
| AWS_DEFAULT_REGION (*) | AWS region. |
| ECS_CLUSTER_NAME (*) | The name of your ECS cluster. |
| ECS_SERVICE_NAME (*) | The name of your ECS service. |
| ECS_TASK_FAMILY_NAME | The name of your task family. If provided, will overwrite the task family name in your task definition json. Default: `null`. |
| ECS_TASK_DEFINITION (*) | Path to the task definition json file. The file should contain a task definition as described in the [AWS docs](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html) |
| DEBUG                 | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Prerequisites

To use this pipe you should have an ECS cluster running a service. Learn how to create one [here](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ECS_GetStarted_EC2.html).

## Examples

Basic example:

```yaml
script:
  - pipe: atlassian/aws-ecs-deploy:0.2.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      ECS_CLUSTER_NAME: 'my-ecs-cluster'
      ECS_SERVICE_NAME: 'my-ecs-service'
      ECS_TASK_DEFINITION: 'task-definition.json'
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community](https://community.atlassian.com/t5/forums/postpage/choose-node/true/interaction-style/qanda?add-tags=bitbucket-pipelines,pipes,aws,ecs).

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
