import json
import os
import sys
from pprint import pformat

import boto3
from bitbucket_pipes_toolkit import Pipe, get_logger
from botocore.exceptions import ClientError, ParamValidationError
from botocore import exceptions as boto_exceptions

logger = get_logger()

schema = {
    "AWS_ACCESS_KEY_ID": {
        "type": "string",
        "required": True
    },
    "AWS_SECRET_ACCESS_KEY": {
        "type": "string",
        "required": True
    },
    "AWS_DEFAULT_REGION": {
        "type": "string",
        "required": True
    },
    "ECS_CLUSTER_NAME": {
        "type": "string",
        "required": True
    },
    "ECS_SERVICE_NAME": {
        "type": "string",
        "required": True
    },
    "ECS_TASK_FAMILY_NAME": {
        "type": "string",
        "required": True
    },
    "ECS_TASK_DEFINITION": {
        "type": "string",
        "required": True
    },
    "DEBUG": {
        "type": "boolean",
        "default": False
    }
}


class ECSDeploy(Pipe):

    def get_client(self):
        try:
            return boto3.client('ecs', region_name=self.get_variable('AWS_DEFAULT_REGION'))
        except ClientError as err:
            self.fail("Failed to create boto3 client.\n" + str(err))

    def _handle_update_service_error(self, error):
        error_code = error.response['Error']['Code']
        if error_code == 'ClusterNotFoundException':
            msg = f'ECS cluster not found. Check your ECS_CLUSTER_NAME.'
        elif error_code == 'ServiceNotFoundException':
            msg = f'ECS service not found. Check your ECS_SERVICE_NAME'
        else:
            msg = f"Failed to update the stack.\n" + str(error)
        self.fail(msg)

    def update_task_definition(self, family, task_definition_file, image=None):

        logger.info(f'Updating the task definition for {family}.')

        client = self.get_client()
        try:
            with open(task_definition_file) as d_file:
                task_definition = json.load(d_file)
        except json.decoder.JSONDecodeError:
            self.fail('Failed to parse the task definition file: invalid JSON provided.')
        except FileNotFoundError:
            self.fail(f'Not able to find {task_definition_file} in your repository.')

        if family is not None:
            task_definition['family'] = family
        logger.info(f'Using task definition: \n{pformat(task_definition)}')

        try:
            response = client.register_task_definition(**task_definition)
            return response['taskDefinition']['taskDefinitionArn']
        except ClientError as err:
            self.fail("Failed to update the stack.\n" + str(err))
        except KeyError as err:
            self.fail("Unable to retrieve taskDefinitionArn key.\n" + str(err))
        except ParamValidationError as err:
            self.fail(f"ECS task definition parameter validation error: \n {err.args[0]}")

    def update_service(self, cluster, service, task_definition):

        logger.info(f'Update the {service} service.')

        client = self.get_client()

        try:
            response = client.update_service(
                cluster=cluster,
                service=service,
                taskDefinition=task_definition,
            )
            return response
        except ClientError as err:
            self._handle_update_service_error(err)

    def run(self):
        super().run()
        family = self.get_variable('ECS_TASK_FAMILY_NAME')
        definition = self.get_variable('ECS_TASK_DEFINITION')
        try:
            image = self.get_variable('IMAGE_NAME')
        except KeyError:
            image = None
        cluster_name = self.get_variable('ECS_CLUSTER_NAME')
        service_name = self.get_variable('ECS_SERVICE_NAME')
        region = self.get_variable('AWS_DEFAULT_REGION')

        task_definition = self.update_task_definition(
            family, definition, image)
        response = self.update_service(
            cluster_name, service_name, task_definition)

        self.success(f'Successfully updated the {service_name} service. You can check you servce here: \n'
                     f'https://console.aws.amazon.com/ecs/home?region={region}#/clusters/{cluster_name}/services/{service_name}/details')

if __name__ == '__main__':
    pipe = ECSDeploy(pipe_metadata='pipe.yml', schema=schema)
    pipe.run()
